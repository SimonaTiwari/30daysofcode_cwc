Write a java program to find next smallest palindrome number.

import java.util.Scanner;
public class ques51 {

	public static void main(String[] args) {
		Scanner sc= new Scanner(System.in);
		System.out.print("Input the number: ");
		int n=sc.nextInt();
		if (n>0)	
			 System.out.println("Next smallest palindrome:" + nextPalindromeGenerate(n));
	}
	
	public static int nextPalindromeGenerate(int n) {
		int ans=1, digit, rev_num=0;                
		if(n<10){   
			return n+1;
		}
		int num=n;
		while(ans!=0){
			rev_num=0;digit=0;
            n=++num;

            while(n>0)     
            {
                digit=n%10;
                rev_num=rev_num*10+digit;
                n=n/10;
            }

            if(rev_num==num)   
            {
                ans=0;
				return num;
            }

            else ans=1;
        }
		return num;
    }
}