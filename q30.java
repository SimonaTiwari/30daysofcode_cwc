//Write a program accept N numbers in an array then shift the positive numbers towards the left and negative elements towards the right without altering the original sequence.

public class ques30 {
	public static void main(String[] args) {
		int arr[] = {-3,22,-45,67,8,10,9,-2,-5};
		int n = arr.length;
		rearrange(arr,n);
		for (int i=0;i<n;i++)
            System.out.print(arr[i]+" ");
	}
	public static int[] rearrange(int arr[], int n){
        int j=0,temp;
        for (int i=0;i<n;i++) {
        	if (arr[i]>0) {
        		if (i!= j) {
        			temp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = temp;
        		}
                j++;
            }
        }
        return arr;
	}
}